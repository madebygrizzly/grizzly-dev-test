# Grizzly Front-End Dev Evaluation

## Part 1 - Static Front-End Development
The Grizzly Front-End Dev Eval is meant to test two separate skills. In the first half of this evaluation, you'll be creating a simple HTML/CSS/JS landing page. This evaluation is meant to explore your individual development style and technique, as well as display your strategic front-end problem-solving and precision. Please feel free to use whichever boilerplate or framework/scaffold you feel most comfortable with; though the use of a framework or scaffold is not a requirement of this project.

There is no time limit for completing this evaluation. Please note that attention to detail is paramount in this exercise, so please be sure that the end product has been QA'd for responsive and cross-browser support (see below for details).

To begin, clone this repo down to your local machine and work from there. When you have completed your test, push your code up to a different private BitBucket or GitHub repository and provide access to Rich (rich@madebygrizzly.com). Finally, once you've completed this project, **deploy your code to a live staging environment of your choice and provide a link for our review**.

As with any project, we believe that communication is a vital part of this test. If you have questions, concerns, or feedback please feel free to reach out to rich@madebygrizzly.com and we'll work with you to clarify and answer questions.

#### Requirements
- The final deliverable is a static landing page.
- The page should be built using SCSS, HTML5, and JavaScript. PHP is also acceptable if desired.
- All of the assets you will need for this project are included in the design-assets folder.
- The fonts used in this project are both Google fonts: Ubuntu (Bold and Regular) and Lato (Black).
- The form should send the user's email address to 'rich@madebygrizzly.com' with the subject 'Dev Test from <your name>', and the form should validate.
- There should be a human-readable README.md file in the repo providing dev setup instructions.
- The carousel should should display a real Instagram feed of your choosing.
- All non-photo design elements should be created in CSS via SCSS.
- Using some sort of asset pipeline (Gulp/Grunt/CodeKit with Bower/npm/other) is required.


## Part 2: WordPress Integration
The second half of the Grizzly Front-End Dev Evaluation is to take Part 1 of the eval and turn it into a fully functioning WordPress site, complete with editable content input for the page. Please make sure that the theme you create is set up "The WordPress Way", using the default WordPress template hierarchy. Additionally, please keep the template files as clean as possible, using the functions.php file to hold any functionality alterations or updates (again, the "WordPress Way"). Finally, please ensure that the page's content is editable via a customized admin page using the custom field functionality of your choice (Advanced Custom Fields, Pods, CMB2 etc.).

#### Requirements
- The final deliverable is a functioning WordPress site built upon the same code written for the part 1.
- All .js and .css files should be enqueued properly.
- The README.md file should be updated to include any relevant project information for the WordPress Site including db creds, access information and any configuration instructions necessary to render the site completely on the reviewer's local machine.
- The repo containing the site should include a 'db' folder including the final database for the WordPress site.
- All PHP should be error-free and written according to best practices.


## Additional Considerations:

#### Responsive Considerations
When building out the front end, please ensure that your end-product is as visually put-together as possible. In addition to developing a beautiful desktop experience, we're also very much looking at how you choose to implement responsive design to maximize the experience on smaller (and larger) screens, and everything in between.

#### Instagram carousel
Feel free to use the JavaScript/jQuery carousel plugin of your choice. Please ensure that the carousel consists of 8 images on an endless loop, with each image linking out to the original Instagram post.

#### Form Styling
The form should be styled exactly the way it is seen in the comp, and should not use any default browser or device styling. The form should have simple validation on it, and should include a success confirmation of some sort.

# Evaluatory Criteria

#### Code == Design
The implementation of design on the web should never sacrifice that design's integrity. Development is about bringing interaction design to life, and our developers are as integral to this process as our designers. Development shouldn't be restrictive, and we encourage our developers to work with our designers and strategists to create amazing interactive experiences. This means that you are invited and encouraged to breathe life and excitement into the page through interaction and precision.

#### Attention to Detail
**This is huge**. We expect that all code written for/by Grizzly has been tested, is cross-browser compatible, and is optimized for the best user experience. Assets should be minified/compressed, code should be modular and scalable, and the intent of the design should be reflected in the final deliverable.

#### Code Quality
This is another huge component of the Grizzly ethos. We will be looking through every line of submitted code, and will be looking for best-practices PHP, JavaScript, SCSS or SASS, and HTML. Keep your code clean, readable, well-organized, and document/comment as much as possible.

#### Commit Quality
We share code a lot here, and as such it's very important to commit often and to include informative, helpful commit messages. This makes everyone's job easier down the road, and it certainly makes tracking down a bug or error much much less cumbersome.

#### Grizzly Vibes
We get things done, but we have fun. We are a community of open communication. If something is unclear, please ask questions. Most importantly enjoy what you do, have fun, and happy coding!
